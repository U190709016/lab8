package shapes3d;

public class Cube extends shapes2d.Square {

    public Cube(int side){
        super(side);

    }
    public double volume(){
        return super.area()* super.side;
    }

    @Override
    public double area() {
        return 6*super.area();
    }

    @Override
    public String toString() {
        return "Side of the cube = "+super.side;
    }
}
