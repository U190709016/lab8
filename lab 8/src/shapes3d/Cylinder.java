package shapes3d;

public class Cylinder extends shapes2d.Circle{
    public int height;
    public Cylinder(int radius,int height) {
        super(radius);
        this.height=height;
    }
    public double area(){
        return 2*super.area() + 2*super.pi*super.radius*height;
    }
    public double volume(){
        return super.area()*height;
    }
    public String toString(){
        return "Height of the Cylinder = "+height+"\n"+"Radius of the Cylinder = " + super.radius ;
    }
}
