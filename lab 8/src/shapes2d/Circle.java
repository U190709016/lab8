package shapes2d;

public class Circle {
    public double pi = Math.PI;
    public int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public double area(){
        return radius*radius*pi;
    }

    @Override
    public String toString() {
        return "Radius of cirle= "+radius;
    }
}

