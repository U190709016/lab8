package shapes2d;


public class Square {
    public int side;

    public Square(int side){
        this.side=side;
    }
    public double area(){
        return side*side;
    }
    public String toString() {
        return "Side of the square = " + side;
    }
}
